<?php 
	session_start(); 
	$db = mysqli_connect('localhost', 'root', '', 'dbvalidar');
	if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: index.php');
	}
	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['username']);
		header("location: index.php");
	}
?>
<!DOCTYPE html> 
<html lang="es"> 
	<head>
    	<title> Modificación datos</title>
    	<?php include('head.php');?>
	</head>
  	<body> 
		<div class="container-fluid">
	  		<div class="row">
	    		<div class="col-lg-8">
	    			<strong><p>Idioma:</p></strong>
	 				<?php include('idioma.php') ?>
	      			<div class="rregistro"> 
	      				<h1>Modificación datos</h1>
	      				<?php if (isset($_SESSION['success'])) : ?>
						<div class="error success" >
							<h3>
								<?php 
									echo $_SESSION['success']; 
									unset($_SESSION['success']);
								?>
							</h3>
						</div>
						<?php endif ?>
						<!-- logged in user information -->
						<?php  if (isset($_SESSION['username'])) : ?>
						<h2 class="welcome" >Hola, <strong><?php echo $_SESSION['username']; ?></strong></h2>
	        			<p class="welcome">Tus datos son:</p>
	        			<p align="center"> <a href="home.php" style="color: red;">Volver al Inicio</a> </p>
	        			<div class="info">
	        					<?php 
	        						$username = $_SESSION['username'];
	        						$query = $sql = "SELECT * FROM registro WHERE username = '$username'";
									$results = mysqli_query($db, $query);
	        						$row = mysqli_fetch_assoc($results);
	        					?>
	        					<form method="post" action="server.php">
	        						<div class="form-group">
				    					<div class="col-md-offset-2 .col-sm-10" style="display: none;">
				    						<b> ID:</b> <input type="text" name="id" required value="<?php echo ''.$row['id'] ?>">
			        					</div>
	        						</div>
	        						<div class="form-group">
				    					<div class="col-md-offset-2 .col-sm-10">
				    						<b> Email:</b> <input type="text" name="email" required value="<?php echo ''.$row['email'] ?>">
			        					</div>
	        						</div>
	        						<!-- Borrar si no funciona-->	
	        						<div class="form-group">
				    					<div class="col-md-offset-2 .col-sm-10">
				    						<b> Género:</b>
										    <label><input type="radio" class="largerCheckbox" id="genero" name="genero" value="Hombre" <?php if ($row['genero'] ==  "Hombre") { echo  "checked"; } ?> >  Hombre</label>
										    <label><input type="radio" class="largerCheckbox" id="genero" name="genero" value="Mujer"
										    <?php if ($row['genero'] ==  "Mujer") { echo  "checked"; } ?> > Mujer</label>
			        					</div>
	        						</div>	
	        						<div class="form-group">
			        					<div class="col-md-offset-6 .col-sm-10">
				    						<b> Nacimiento:</b> <input type="text" name="nacimiento" required value="<?php echo ''.$row['nacimiento'] ?>">
			        					</div>
	        						</div>
	        						<div class="form-group">
			        					<div class="col-md-offset-6 .col-sm-10">
				    						<b> País:</b> <input type="text" name="pais" required value="<?php echo ''.$row['pais'] ?>">
			        					</div>
	        						</div>
			        				<button type="submit" class="btnedit btn-default" id="modificar_datos" name="modificar_datos">Confirmar datos</button>
	        					</form>
	        					<!-- Closed sesion -->
	        					<form method="post" action="home.php">
	          						<div class="form-group">
	             						<button type="submit" class="btn-logout" style="color: red id="logout" name="logout">x Cerrar sesión</button>
	          						</div>
	        					</form>	
								<?php endif ?>
						</div>		
	      			</div>
	    		</div>
	    		<div class="col-lg-4">
		 			<a href="/registro/home.php" id="but-x" class="close">&times;</a>
	      			<div class="image-home"></div>
	    		</div>
	  		</div>
		</div> 
	</body> 
  	<footer><?php include('footer.php');?></footer>
</html> 