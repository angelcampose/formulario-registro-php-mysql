<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <!– Bootstrap CSS –>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!– Importante llamar antes a jQuery para que funcione bootstrap.min.js   –> 
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<!– Llamamos al JavaScript de Bootstrap –> 
  	<script src="js/bootstrap.min.js"></script>
    <!–Con esto garantizamos que se vea bien en dispositivos móviles–>
    <link rel="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!– Mis estilos  –>
    <link rel="stylesheet" type="text/css" href="style.css">
</head> 