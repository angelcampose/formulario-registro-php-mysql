<?php include('server.php') ?>
<!DOCTYPE html> 
<html lang="es"> 
	<head>
		<title> Registro</title>
	    <?php include('head.php');?>
	</head>
  	<body> 
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-8">
			    	<strong><p>Idioma:</p></strong>
			 		<?php include('idioma.php') ?>
				    <div class="rregistro"> 
				      	<h1>Registro</h1>
					    <form class="form-horizontal" method="post" action="registro.php">
					        <?php include('errors.php'); ?>
							<div class="form-group">
								<label class="control-label col-sm-5" for="username">Username</label>
								<div class="col-sm-12">
								    <input type="text" class="form-control" id="username" name="username" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-5" for="email">Correo electrónico</label>
								<div class="col-sm-12">
									<input type="email" class="form-control" id="email" name="email" required>
								</div>
							</div>
							<div class="form-group">
							    <label class="control-label col-sm-5" for="pwd">Contraseña</label>
							    <div class="col-sm-12">
							    	<input type="password" class="form-control" id="password" name="password" required>
							    </div>
							</div>
							<div class="form-group">
							    <label class="control-label col-sm-5" for="rpassword">Repetir contraseña</label>
							    <div class="col-sm-12">
							    	<input type="password" class="form-control" id="rpassword" name="rpassword" required>
							    </div>
							</div>
							<div class="form-group">
							    <div class="col-sm-12">
							    	<div class="checkbox" required>
								      	<p>Genero</p>
								        <label><input type="radio" class="largerCheckbox" id="genero" name="genero" value="Hombre" > Hombre</label>
								        <label><input type="radio" class="largerCheckbox" id="genero" name="genero" value="Mujer" > Mujer</label>
							      	</div>
								</div>
							</div>
							 <div class="form-group">
							    <label class="control-label col-sm-3" for="nacimiento">Nacimiento</label>
							    <div class="col-sm-12">
							    	<input type="text" class="form-control" id="nacimiento" name="nacimiento" required>
							    </div>
							 </div>
							<div class="form-group">
								<div class="col-sm-12">
									<label for="sel1">Pais/Residencia</label>
									<select class="form-control" id="pais" name="pais">
									    <option>España</option>
									    <option>Catalunya </option>
									    <option>Reino Unido</option>
								  	</select>
								</div> 
							</div>
							<div class="form-group">
								<div class="col-sm-12">
							    	<div class="checkbox">
							        	<label><input type="checkbox" class="largerCheckbox" name="condiciones" required> Aceptar cookies</label>
							      	</div>
							    </div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
							    	<button type="submit" class="btn btn-default" id="reg_user" name="reg_user">Registrarme</button>
							    </div>
							</div>
							<div class="form-group">
							    <div class="col-sm-12">
							    	<button type="reset" id="btn-borrar" class="btn btn-default">Borrar</button>
							    </div>
							</div>
						</form> 
				    </div>
			    </div>
			    <div class="col-lg-4">
					<a href="/registro/index.php" id="but-x" class="close">&times;</a>
			      	<div class="image-registro"></div>
			    </div>
		  	</div>
		</div> 
	</body> 
  <footer><?php include('footer.php');?></footer>
</html> 