<?php 
    session_start();

    // variable declaration
    $username = "";
    $email    = "";
    $errors = array(); 
    $_SESSION['success'] = "";

    // connect to database
    $db = mysqli_connect('localhost', 'root', '', 'dbvalidar');

    // REGISTER USER
    if (isset($_POST['reg_user'])) {
        // receive all input values from the form
        $username = mysqli_real_escape_string($db, $_POST['username']);
        $email = mysqli_real_escape_string($db, $_POST['email']);
        $password = mysqli_real_escape_string($db, $_POST['password']);
        $rpassword = mysqli_real_escape_string($db, $_POST['rpassword']);
        $genero = mysqli_real_escape_string($db, $_POST['genero']);
        $nacimiento = mysqli_real_escape_string($db, $_POST['nacimiento']);
        $pais = mysqli_real_escape_string($db, $_POST['pais']);
        
        // form validation: ensure that the form is correctly filled
        if (empty($username)) { array_push($errors, "Username is required"); }
        if (empty($email)) { array_push($errors, "Email is required"); }
        if (empty($password)) { array_push($errors, "Password is required"); }

        if ($password != $rpassword) {
            array_push($errors, "The two passwords do not match");
        }

        // register user if there are no errors in the form
        if (count($errors) == 0) {
            $password = md5($password);//encrypt the password before saving in the database
            $query = "INSERT INTO registro (username, email, password,genero,nacimiento,pais) 
                      VALUES('$username', '$email', '$password', '$genero', '$nacimiento', '$pais')";
            mysqli_query($db, $query);

            $_SESSION['username'] = $username;
            $_SESSION['email'] = $email;
            $_SESSION['genero'] = $genero;
            $_SESSION['nacimiento'] = $nacimiento;
            $_SESSION['pais'] = $pais;
            $_SESSION['success'] = "Estas dins del sistema";
            header('location: home.php');
        }
    }
    // ... 
    // LOGIN USER
    if (isset($_POST['login_user'])) {
        $username = mysqli_real_escape_string($db, $_POST['username']);
        $password = mysqli_real_escape_string($db, $_POST['password']);
        // $recuerda = mysqli_real_escape_string($db, $_POST['recuerda']);
        if (empty($username)) {
            array_push($errors, "Username is required");
        }
        if (empty($password)) {
            array_push($errors, "Password is required");
        }

        if (count($errors) == 0) {
            $password = md5($password);
    
            $query = "SELECT * FROM registro WHERE username='$username' AND password='$password'";

            $results = mysqli_query($db, $query);

            if (mysqli_num_rows($results) == 1) {
                $_SESSION['username'] = $username;
                $_SESSION['success'] = "Ahora está conectado";
                header('location: home.php');
            }else {
                array_push($errors, "Wrong username/password combination");
            }
        }
        
      /*if(!empty($_POST['recuerda'])) {
                setcookie ("member_login",$_POST['username'],time()+ (10 * 365 * 24 * 60 * 60));
                setcookie ("member_password",$_POST['password'],time()+ (10 * 365 * 24 * 60 * 60));
            } else {
                if(isset($_COOKIE['member_login'])) {
                    setcookie ('member_login','');
                }
                if(isset($_COOKIE['member_password'])) {
                    setcookie ('member_password','');
                }
           
              } 
      */   
    }

    //Modificar datos
      if (isset($_POST['modificar_datos'])) {
        $id = mysqli_real_escape_string($db, $_POST['id']); 
        $email = mysqli_real_escape_string($db, $_POST['email']);
        $genero = mysqli_real_escape_string($db, $_POST['genero']);
        $nacimiento = mysqli_real_escape_string($db, $_POST['nacimiento']);
        $pais = mysqli_real_escape_string($db, $_POST['pais']);


        $sql = "SELECT * FROM agenda WHERE id = $id";
        $results = mysqli_query($db, $query);
    
        $query = "UPDATE registro SET email='$email', genero='$genero', nacimiento='$nacimiento', pais='$pais' WHERE id='$id'";
        $results = mysqli_query($db, $query);

        if($results){
          /*
            $_SESSION['email'] = $email;
            $_SESSION['genero'] = $genero;
            $_SESSION['nacimiento'] = $nacimiento;
            $_SESSION['pais'] = $pais;
            */
            $_SESSION['success'] = "Se han modificado los datos correctamente";
            header('location: home.php'); 
          }
        else{
          echo "La modificación de datos no fue exitosa";
        }
      }
?>