<?php include('server.php') ?>
<!DOCTYPE html> 
<html lang="es"> 
	<head>
	    <title> Login</title>
	    <?php include('head.php');?>
	</head>
	<body> 
		<div class="container-fluid"> 
			<div class="row">
				<div class="col-lg-8">
			    	<strong><p>Idioma:</p></strong>
			    	<?php include('idioma.php') ?>
			      	<div class="datos"> 
				    	<h1>Iniciar Sesión</h1>
						<form class="form-horizontal" method="post" action="index.php">
							<?php include('errors.php'); ?>
							<div class="form-group">
								<label class="control-label col-sm-2" for="username"></label>
							    <div class="col-sm-12">
							    	<input type="text" class="form-control" id="username" name="username" placeholder="Nombre de usuario">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="password"></label>
							    <div class="col-sm-12">
							      <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
							    </div>
							</div>
							<!-- recuerdame disable -->
							<!--
							<div class="form-group">
							    <div class="col-sm-12">
							    	<div class="checkbox">
							    		<label><input type="checkbox" class="largerCheckbox" id="recuerda" name="recuerda"> Recuerdame</label>
							      	</div>
							    </div>
							</div>
							-->
							<div class="form-group">
								<div class="col-sm-12">
							    	<button type="submit" class="btn btn-default" name="login_user">Entrar</button>
								</div>
							</div>
							<div class="textregistro">
							    <div class="col-sm-12">
							    	<p> No estas registrado ? <b><a  href="/registro/registro.php">Crear cuenta</a></b></p>
							    </div>
							</div>
						</form> 
			      	</div>
			    </div>
			    <div class="col-lg-4">
			      <div class="image"></div>
				</div>
			</div>
		</div> 
	</body>
	<footer><?php include('footer.php');?></footer> 
</html> 