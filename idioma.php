<?php  
	if (isset($_POST['Llengua'])){
		if ($_POST['Llengua']=="castella"){
		    setcookie("Bandera","castella",time()+ (10 * 365 * 24 * 60 * 60));
		}
		elseif ($_POST['Llengua']=="catala"){
		    setcookie ("Bandera","catala",time()+ (10 * 365 * 24 * 60 * 60));
		}
		elseif ($_POST['Llengua']=="ingles"){
		    setcookie ("Bandera","ingles",time()+ (10 * 365 * 24 * 60 * 60));
		}
		header("Location:".$_SERVER['PHP_SELF']);
	}		    	
	if (isset($_COOKIE['Bandera'])){
		if ($_COOKIE["Bandera"]=="castella"){?> 
			<img class="bandera" src="img/spain.jpg">
			<form action="<?php $_SERVER ['PHP_SELF']?>" id="Llenguaform" method="POST">
			    <select id="Llengua" name="Llengua">
				    <option value="castella" selected>Castella</option>
				    <option value="catala">Catala</option>
				    <option value="ingles">Ingles</option>
			    </select>
			    <link rel="stylesheet" href="boton.css"/>
			    <input type="submit" id="enviar" name="enviar" value="Cambiar idioma">
			</form>
			<?php 
		}
		if ($_COOKIE["Bandera"]=="catala"){?> 
			<img class="bandera" src="img/catalan.jpg">
			<form action="<?php $_SERVER ['PHP_SELF']?>" id="Llenguaform" method="POST">
			    <select id="Llengua" name="Llengua">
				    <option value="catala" selected>Catala</option>
				    <option value="castella">Castella</option>
				    <option value="ingles">Ingles</option>
			    </select>
				<link rel="stylesheet" href="boton.css"/>
				<input type="submit" id="enviar" name="enviar" value="Cambiar idioma">
			</form>
			<?php
		}
		if ($_COOKIE["Bandera"]=="ingles"){?> 
			<img class="bandera" src="img/english.jpg">
			<form action="<?php $_SERVER ['PHP_SELF']?>" id="Llenguaform" method="POST">
			    <select id="Llengua" name="Llengua">
				    <option value="ingles" selected>Ingles</option>
				    <option value="castella">Castella</option>
				    <option value="catala">Catala</option>
			    </select>
			    <link rel="stylesheet" href="boton.css"/>
			    <input type="submit" id="enviar" name="enviar" value="Cambiar idioma">
			</form>
			<?php
		}
	}
	else {?>
		<img class="bandera" src="img/spain.jpg">
		<form action="<?php $_SERVER ['PHP_SELF']?>" id="Llenguaform" method="POST">
			<select id="Llengua" name="Llengua">
				<option value="castella" selected>Castella</option>
				<option value="catala">Catala</option>
				<option value="ingles">Ingles</option>
			</select>
			<link rel="stylesheet" href="boton.css"/>
			<input type="submit" id="enviar" name="enviar" value="Cambiar idioma">
		</form>	
		<?php
	}	
?>