<?php 
  session_start(); 
  $db = mysqli_connect('localhost', 'root', '', 'dbvalidar');
	if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: index.php');
	}
	if (isset($_POST['logout'])) {
		session_destroy();
		// unset($_SESSION['username']);
		header("location: index.php");
	}
?>
<!DOCTYPE html> 
<html lang="es"> 
  <head>
    <title> Home</title>
    <?php include('head.php');?>
  </head>
  <body> 
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-8">
          <strong><p>Idioma:</p></strong>
          <?php include('idioma.php') ?>
          <div class="rregistro"> 
        	  <h1>Inicio</h1>
        	  <?php if (isset($_SESSION['success'])) : ?>
            <div class="error success" >
  				    <h3>
  					   <?php 
  						    echo $_SESSION['success']; 
  						    unset($_SESSION['success']);
  				      ?>
  				    </h3>
            </div>
  		      <?php endif ?>
          		<!-- logged in user information -->
          	<?php  if (isset($_SESSION['username'])) : ?>
          	<h2 class="welcome" >Bienvenido <strong><?php echo $_SESSION['username']; ?></strong></h2>
            <p class="welcome">Tus datos son los siguientes:</p>
            <p align="center"> <a href="home_editar.php" style="color: red;">Modificar datos</a> </p>
            <div class="info">
              <?php 
                $username = $_SESSION['username'];
                $query = $sql = "SELECT * FROM registro WHERE username = '$username'";
            		$results = mysqli_query($db, $query);
                $row = mysqli_fetch_assoc($results);
                echo '<strong>Nombre de usuario: </strong>' . $row['username'];
                echo '<br><strong>Email: </strong>' . $row['email'];
                echo '<br><strong>Género: </strong>' . $row['genero'];
                echo '<br><strong>Nacimiento: </strong>' . $row['nacimiento'];
                echo '<br><strong>País: </strong>' . $row['pais'];
              ?>
              <form method="post" action="home.php">
                <div class="form-group">
                  <button type="submit" class="btn-logout" id="logout" name="logout">x Cerrar sesión</button>
                </div>
              </form>
          		<?php endif ?>
          	</div>		
          </div>
        </div>
        <div class="col-lg-4">
          <div class="image-home"></div>
        </div>
      </div>
    </div> 
  </body>
  <footer><?php include('footer.php');?></footer>
</html> 